#include "xspline.c"
#include <R_ext/GraphicsEngine.h>
#include <Rcpp.h>
using namespace Rcpp;


// [[Rcpp::export]]
int timesTwo(int x) {
	return(x * 2);
}

/*** R
	timesTwo(4)
*/
