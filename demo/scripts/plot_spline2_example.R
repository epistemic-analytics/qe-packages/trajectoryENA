# devtools::install_local("../rENA", force = TRUE)

library(rENA)
library(ggplot2)

file = read.csv(system.file('extdata', 'data.csv', package = "trajectoryENA"))

set = rENA::ena(
  data = file,
  units = c("Group","Time","Speaker"),
  conversation = c("Group","Round"),
  codes = c("Disagreement","Agreement","Hypothesis","Falsifying.Evidence","Supporting.Evidence","Refutation"),
  metadata = NULL,
  model = "EndPoint",
  window.size.back = 4,
  weight.by = "binary",
  points = FALSE,
  include.plots = FALSE
);

trajectory_plot(set, group_col = "Group", to_plot = c(1:2), x = "Time", y = "SVD2") +
  trajectory_plot(set, group_col = "Group", to_plot = c(1:2), x = "SVD1", y = "SVD2") +
	trajectory_plot(set, group_col = "Group", to_plot = c(1:2), x = "SVD1", y = "Time") +
	theme_trajectories() +
  trajectory_layout()
;

# set$points$Group <- as.factor(set$points$Group)
# set$points$Time <- as.factor(set$points$Time)

dat <- set$points[Group %in% 1:2]

grp_cis <- lapply(split(dat, by = c("Group"), drop = TRUE), function(x) {
    r <- split(x, by = c("Time"))
    lapply(r, function(s) {
        t.test(s[,c("SVD1", "SVD2")])$conf.int
    })
})
grp_means <- dat[, lapply(.SD, mean), by = c("Group", "Time"), .SDcols = c("SVD1", "SVD2")];
# grp_cis_m <- data.frame(t(as.matrix(data.frame(grp_cis[[1]]))))
grp_cis_m <- data.table::rbindlist(lapply(grp_cis, function(x) data.frame(t(as.matrix(data.frame(x))))));
colnames(grp_cis_m) <- c("CI1", "CI2")
grp_cis_m$Group = unlist(lapply(unique(dat$Group), rep, length(unique(dat$Time))))
grp_cis_m$Time = rep(unique(dat$Time), length(unique(dat$Group)))
grp_mrg <- merge(grp_cis_m, grp_means, by = c("Group", "Time"))
grp_mrg$alpha <- scales::rescale(grp_mrg$Time, to = c(0.25, 1))
#
# ggplot(grp_means, aes(Time, SVD2, group = Group, frame = Time, color = Group)) +
#   traj_means(aes(size = Time)) + traj_paths() +
#   geom_errorbar(aes(ymin = SVD2 - CI1, ymax = SVD2 + CI1, alpha = alpha), data = grp_mrg, width=.1) +
# 	theme_trajectories();

# ggplot(grp_means, aes(Time, SVD2, group = Group, frame = Time, color = Group)) +
#   traj_means(aes(size = Time)) + traj_paths() +
#   geom_errorbar(aes(ymin = SVD2 - CI1, ymax = SVD2 + CI1), data = grp_mrg, width=.1) +
# 	theme_trajectories();
#
# ggplot(dat[Group == 1], aes(Time, SVD2, group = Speaker, color = Speaker, frame = Time)) +
#   traj_points(aes(size = Time)) + traj_paths(aes(alpha = 0.5)) +
#   traj_means(aes(size = Time, group = Group, color = Group)) + traj_paths(aes(Time, SVD2, group = Group, color = Group, frame = Time))


g <- ggplot(grp_means, aes(Time, SVD2, group = Group, color = Group, frame = Time)) +
    traj_means(aes(size = Time)) + traj_paths() +
		theme_trajectories() +
	ggplot(grp_means, aes(SVD1, SVD2, group = Group, color = Group, frame = Time)) +
    traj_means(aes(size = Time)) + traj_paths() +
		theme_trajectories() +
	ggplot(grp_means, aes(SVD1, Time, group = Group, color = Group, frame = Time)) +
    traj_means(aes(size = Time)) + traj_paths() +
		theme_trajectories() +
	trajectory_layout()

print(g)

# ggplot(dat, aes(SVD1, SVD2, group = Group, color = Group, frame = Time)) +
#   traj_points(aes(size = Time)) +
# 	traj_paths()
