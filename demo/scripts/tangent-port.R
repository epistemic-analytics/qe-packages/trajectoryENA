CatmullRomExp = 0.5;
drawing.smoothopen <- function(pts, smoothness) {
    if(nrow(pts) < 3) {
    	return(pts);
    }
    path = matrix(pts[1, ], ncol = 2)

    tangents = array(dim = c(2,2,nrow(pts)-1-1))
    for(i in seq(from = 2, to = nrow(pts) - 1)) {
    	new_tangents <- makeTangent(pts[i - 1, ], pts[i, ], pts[i + 1, ], smoothness);
    	# browser()
    	tangents[,,i-1] <- new_tangents;
    }
    tangents <- na.omit(tangents)
    # browser()
    path <- rbind(path, tangents[1, , 1], pts[2, ]);

    for(i in seq(from = 3, to = nrow(pts) - 1)) {
    	path <- rbind(path, tangents[2, , i -2], tangents[1, , i - 1], pts[i, ])
    }
    # browser()
    # path = path + 'Q' + tangents[pts.length - 3][2] + ' ' + pts[pts.length - 1];
    path <- rbind(path, tangents[2, , nrow(pts) - 2], pts[nrow(pts), ])

    return(list(
    	path = path,
    	points = pts,
    	tangents = tangents
    ));
};


makeTangent <- function(prevpt, thispt, nextpt, smoothness) {
	# browser()
  d1x = prevpt[1] - thispt[1];
  d1y = prevpt[2] - thispt[2];
  d2x = nextpt[1] - thispt[1];
  d2y = nextpt[2] - thispt[2];
  d1a = (d1x * d1x + d1y * d1y) ^ (CatmullRomExp / 2);
  d2a = (d2x * d2x + d2y * d2y) ^ (CatmullRomExp / 2);
  numx = (d2a * d2a * d1x - d1a * d1a * d2x) * smoothness;
  numy = (d2a * d2a * d1y - d1a * d1a * d2y) * smoothness;
  denom1 = 3 * d2a * (d1a + d2a);
  denom2 = 3 * d1a * (d1a + d2a);
  # browser()
  return(matrix(
    c(
      round(thispt[1] + ifelse(denom1, numx / denom1, 0), 2),
      round(thispt[2] + ifelse(denom1, numy / denom1, 0), 2),
      round(thispt[1] - ifelse(denom2, numx / denom2, 0), 2),
      round(thispt[2] - ifelse(denom2, numy / denom2, 0), 2)
    ),
    byrow = TRUE,
    ncol = 2
  ))
}

pts <- matrix(c(38.55, 252.75,
193.27, 17.25,
208.75, 135,
502.72, 17.25,
657.45, 252.75), byrow = TRUE, ncol = 2)

pts <- matrix(c(78.95, 252.75,
415.48, 17.25,
449.13, 135,
1088.52, 17.25,
1425.05, 252.75), byrow = TRUE, ncol = 2)
