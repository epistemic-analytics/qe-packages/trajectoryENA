#' Title
#'
#' @param set TBD
#' @param x TBD
#' @param y TBD
#' @param frame TBD
#' @param frame_invert TBD
#' @param port TBD
#' @param group_col TBD
#' @param plot_options TBD
#' @param to_plot TBD
#' @param viewer TBD
#'
#' @return plot
#' @export
trajectory_viewer <- function(
	set,
	group_col = "Group",
	x = "SVD1", y = "SVD2",
	frame = "Time",
	frame_invert = TRUE,
	port = 4444,
	plot_options = list(
		colors = RColorBrewer::brewer.pal(5, "Set2"),
		points = list(size = 10, opacity = 1),
		lines = list(width = 2, opacity = 0.2, smoothing = 1, simplify = FALSE, shape = "spline")
	),
	to_plot = NULL
	,viewer = shiny::dialogViewer(dialogName = "Trajectory Viewer", width = 1200, height = 800)
) {
	# Global ----
	# _Variables ----
	colors <- plot_options$colors;
	rng <- NULL;
	spl_all_dt <- NULL;
	spl_all_dt2 <- NULL;
	spl_all_dt3 <- NULL;

	# _Plot functions ----
	prep_splines <- function(sp_dat, x_ = x, y_ = y) {
		spl_all <- lapply(sp_dat, function(xx) {
			get_spline(xx[[x_]], xx[[y_]], amount = 10)
		});
		spl_all_dt <- data.table::rbindlist(spl_all, idcol = "group");
		spl_all_dt$size <- (spl_all_dt$type == 1) * plot_options$points$size;
		spl_all_dt$opacity <- plot_options$points$opacity;
		spl_all_dt[, c("color") := colors[as.numeric(.SD[["group"]])], .SDcols = "group"]
		spl_all_dt <- spl_all_dt[, {
	    SD_one <- .SD;
	    SD_one[, SD_one[seq(.GRP)], by = seq_len(nrow(.SD))]
		}, by = group]
		spl_all_dt
	}

	markers <- list(
		size = ~size,
		opacity = ~opacity
	);
	lines <- plot_options$lines;

	side_plot <- function(input, output, session, dat, vals, time_axis = "y", rng = 1) {
	  output$plot <- plotly::renderPlotly({
	  	dat_ <- vals[[dat]];
	  	if(nrow(dat_) == 0) {
	  		return(NULL);
	  	}

	  	height = 400;
	  	width = 400;
	  	axes <- list(
			  title = "",
			  zeroline = TRUE,
			  showline = FALSE,
			  showticklabels = TRUE,
			  showgrid = FALSE,
			  range = c(-rng, rng)
			)

			dat_inv <- data.table::copy(dat_);
	  	time_rng <- c(0, max(dat_inv$frame) * 1.2);
	  	if( frame_invert == TRUE ) {
	  		time_rng <-rev(c(0, max(dat_inv$frame) * 1.2)) * -1;
	  	}
	  	if(time_axis == "y") {
	  		height = 132
	  		width = 412;
	  		if(frame_invert == TRUE) {
	  			dat_inv$y <- dat_inv$y * -1;
	  		}

	  		yaxis <- axes;
	  		yaxis$range <- time_rng;
	  		yaxis$zeroline = FALSE;
	  		xaxis <- axes;
	  		xaxis$showticklabels = FALSE;
	  	}
	  	else {
	  		height = 412
	  		width = 260
	  		if(frame_invert == TRUE) {
	  			dat_inv$x <- dat_inv$x * -1;
	  		}
	  		xaxis <- axes;
	  		xaxis$range <- time_rng;
	  		xaxis$zeroline = FALSE;
	  		yaxis <- axes;
	  		yaxis$showticklabels = FALSE;
	  	}

			groups <- unique(dat_[["group"]]);
			fig <- plotly::plot_ly(dat_inv[group == groups[[1]],],
				type = "scatter",
		    x = ~x,
		    y = ~y,
		  	height = height,
		  	width = width,
    		frame = ~seq_len,
		    mode = 'markers+lines',
				marker = markers,
				line = lines
		  )

			for( i in seq(length(groups))[-1] ) {
				fig %<>% plotly::add_trace(
					data = dat_inv[group == groups[i], ],
					x = ~x, y = ~y,
					mode = 'markers+lines'
				)
			}

  		fig %<>% plotly::layout(
		    xaxis = xaxis,
  			yaxis = yaxis,
  			autosize = T,
  			showlegend = FALSE,
  			margin = list(t = 0, r = 0, b = 0, l = 0, pad = 0)
		  );

			fig %<>% plotly::animation_slider(hide = TRUE);
			fig$x$layout$updatemenus[[1]]$buttons <- NULL;

			fig
	  })

	  return(NULL)
	};
	main_plot <- function(input, output, session, dat, vals, rng = 1) {
	  output$plot <- plotly::renderPlotly({
	  	dat_ <- vals[[dat]];
	  	if(nrow(dat_) == 0) {
	  		return(NULL);
	  	}

	  	height = 400;
	  	width = 400;
	  	axes <- list(
			  title = "",
			  zeroline = TRUE,
			  showline = FALSE,
			  showticklabels = TRUE,
			  showgrid = FALSE,
			  range = c(-rng, rng)
			)

			groups <- unique(dat_[["group"]]);
			fig <- plotly::plot_ly(dat_[group == groups[[1]],],
					type = "scatter",
			    x = ~x,
			    y = ~y,
	    		frame = ~seq_len,
			    mode = 'markers+lines',
					# name = ~group,
					text = ~paste0("Point: ", group),
			  	height = height,
			  	width = width,
					marker = markers,
					line = lines
			  )
			;

			for( i in seq(length(groups))[-1] ) {
				fig %<>% plotly::add_trace(data = dat_[group == groups[i], ], x=~x, y = ~y, mode = 'markers+lines');
			}
			fig %<>%
	  		plotly::layout(
	  			showlegend = FALSE,
	    		xaxis = axes,
	    		yaxis = axes
	    	) %>% plotly::animation_slider(hide = TRUE)
			;
			fig$x$layout$updatemenus[[1]]$buttons <- NULL
			fig
	  })

	  return(NULL)
	};


	# UI ----
	# _Functions ----
	side_plot_ui <- function(id) {
	  ns <- shiny::NS(id);
	  shiny::tags$div(
		  plotly::plotlyOutput(ns("plot"))
	  );
	};
	main_plot_ui <- function(id) {
	  ns <- shiny::NS(id);
	  shiny::tags$div(
		  plotly::plotlyOutput(ns("plot"))
	  );
	};
	optionsFormUI <- function(id) {
		ns <- shiny::NS(id);
		shiny::inputPanel(
			shiny::textInput("dataframe", "Data Frame")
		)
	};
	optionsForm <- function(input, output, session) {}

	# _Elements ----
	ui <- shiny::fixedPage(
		# __JavaScript ----
		shiny::tags$script(shiny::HTML('
			Shiny.addCustomMessageHandler("animate_plots", (plot) => {
		    let opts = {
		    	mode: "immediate",
	        fromcurrent: true,
			    transition: {
			      duration: 0,
			      easing: "linear",
	        	ordering: "layout first"
			    },
			    frame: {
			      duration: 40,
			      redraw: false
			    }
		    }

			  if( !Array.isArray(plot.frame) ) {
			  	plot.frame = [ plot.frame ];
			  }
		    if(document.getElementById("scatters-plot")) {
		    	Plotly.animate("scatters-plot", plot.frame, opts)
		    }
		    if(document.getElementById("scatters-sidex-plot")) {
		    	Plotly.animate("scatters-sidex-plot", plot.frame, opts)
		    }
		    if(document.getElementById("scatters-sidey-plot")) {
		    	Plotly.animate("scatters-sidey-plot", plot.frame, opts)
		    }
		  });
		  jQuery(document).ready( () => {
		  	console.log("READY");
		  	jQuery("#play-all").on("click", (event) => {
		  		console.log("Playing all....");
		  		let
		  			opts = {
				    	mode: "immediate",
		          fromcurrent: true,
					    transition: {
					      duration: 0,
					      easing: "linear",
		          	ordering: "layout first"
					    },
					    frame: {
					      duration: 40,
					      redraw: false
					    }
					  },
		  			frames = [...Array(56).keys()].map( a => a + 1 )
		  		;
			    if(document.getElementById("scatters-plot")) {
			    	Plotly.animate("scatters-plot", frames, opts)
			    }
			    if(document.getElementById("scatters-sidex-plot")) {
			    	Plotly.animate("scatters-sidex-plot", frames, opts)
			    }
			    if(document.getElementById("scatters-sidey-plot")) {
			    	Plotly.animate("scatters-sidey-plot", frames, opts)
			    }
		  	})
		  })
		')),

		# __CSS ----
		shiny::includeCSS("inst/app/www/styles.css"),

		# __HTML Body ----
	  #shiny::tags$h2("Trajectory Plot"),

	  shiny::tags$div(id="plot-wrapper",
		  side_plot_ui("scatters-sidey"),
		  main_plot_ui("scatters"),
	  	optionsForm("trajectory_options"),
		  side_plot_ui("scatters-sidex")
	 	),

		shiny::tags$div(id = "slider-wrapper",
			shiny::tags$button(id = "play-all", "Play All"),
	  	# shiny::sliderInput("frame", "Frame:", min = 0, step = 1, max = length(unique(spl_1$idx)), value = 0 )
	  	# shiny::sliderInput("frame", "Frame:", min = 1, step = 1, max = 56, value = 1 )
	  	shiny::sliderInput("frame", "Frame:", min = 1, step = 1, max = 6, value = 1 ),
			shiny::checkboxGroupInput("to_plot", "To Plot: ")
		),

	)
	# UI: END ----

	# Server ----
	# _Function ----
	server <- function(input, output, session) {
		# __Pre-Setup ----
		if(is.null(to_plot)) {
			to_plot <- names(sps);
		}

		# __Reactive Values ----
		vals <- shiny::reactiveValues(
			to_plot = to_plot,
			frame = 1,
			current_frame = 1,
			main_df = prep_splines(sps[to_plot]),
			time_y_df = prep_splines(sps2[to_plot], x_ = c(x), y_ = c(frame)),
			time_x_df = prep_splines(sps3[to_plot], x_ = c(frame), y_ = c(y)),
			range = 1
		);

		# __Modules ----
	  shiny::callModule(main_plot, "scatters", "main_df", vals, rng = vals$range);
		shiny::callModule(side_plot, "scatters-sidex", "time_y_df", vals, time_axis = "y", rng = vals$range);
		shiny::callModule(side_plot, "scatters-sidey", "time_x_df", vals, time_axis = "x", rng = vals$range);

		# __Events ----
	  shiny::observeEvent(input$frame, {
			vals$frame <- input$frame;
			new_frame <- 1 + ((11 * (vals$frame - 1)));
			new_frames <- (vals$current_frame:new_frame);
			session$sendCustomMessage("animate_plots", list(frame = new_frames, id = NULL));
			vals$current_frame <- new_frame;
	  }, ignoreInit = TRUE, ignoreNULL = TRUE);
	  shiny::observeEvent(input$to_plot, {
	  	if(! identical(input$to_plot, vals$to_plot) ) {
	  		vals$to_plot <- input$to_plot;
	  		vals$main_df <- prep_splines(sps[vals$to_plot]);
				vals$time_y_df <- prep_splines(sps2[vals$to_plot], x_ = c(x), y_ = c(frame));
				vals$time_x_df <- prep_splines(sps3[vals$to_plot], x_ = c(frame), y_ = c(y));

	  		vals$range <- max(abs(vals$main_df[, c("x", "y")])) * 1.2;
				shiny::updateSliderInput(session, "frame", max = length(unique(vals$main_df$idx)));
	  	}
	  }); #, ignoreInit = TRUE, ignoreNULL = TRUE);

		# __Updates ----
		shiny::updateCheckboxGroupInput(session, "to_plot", choices = names(sps), selected = to_plot);
	};
	# Server: END ----


	# Application ----
	# _Setup ----
	sps  <- spline_points(set, group_col = group_col, time_col = frame);
	sps2 <- spline_points(set, dims = c(x, frame), time_col = frame);
	sps3 <- spline_points(set, dims = c(frame, y), time_col = frame);

	# _Execute ----
	# browser()
	shiny::runGadget(app = ui, server = server, port = port, viewer = viewer);
	# Application: END ----
}
