Package: trajectoryENA
Title: ENA Trajectories
Type: Package
Author: Cody L Marquart [aut, cre] (<https://orcid.org/0000-0002-3387-6792>), Jais Brohinsky [aut], David Williamson Shaffer [aut]
Authors@R: c(person("Cody L", "Marquart", role = c("aut", "cre"), email="cody.marquart@wisc.edu", comment = c(ORCID = "0000-0002-3387-6792")), person("Jais", "Brohinsky", role = c("aut"), email="brohinsky@wisc.edu"), person("David Williamson", "Shaffer", role = c("aut"), email = "dws@education.wisc.edu"))
Maintainer: Cody L Marquart <cody.marquart@wisc.edu>
Version: 0.1.0
Description: ENA (Shaffer, D. W. (2017) Quantitative Ethnography. ISBN: 0578191687) is a method used to identify meaningful and quantifiable patterns in discourse or reasoning. ENA moves beyond the traditional frequency-based assessments by examining the structure of the co-occurrence, or connections in coded data. This package builds upon the rENA package, creating step frame animations of ENA plots.
LazyData: TRUE
Depends:
    R (>= 3.5.0)
License: GPL-3 | file LICENSE
Imports:
    rENA (>= 0.2.0.0),
    data.table,
    ggplot2,
    patchwork
Suggests:
    testthat (>= 2.1.0),
    knitr,
    rmarkdown,
    RColorBrewer
RoxygenNote: 7.1.1
VignetteBuilder: knitr
