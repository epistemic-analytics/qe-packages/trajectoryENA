# Title
#
# @param p0 TBD
# @param p1 TBD
#
# @return numeric
# @export
dist <- function(p0, p1) {
	sqrt( ((p1[1] - p0[1]) ^ 2) + ((p1[2] - p0[2]) ^ 2) );
};

# Title
#
# @param xs TBD
# @param ys TBD
# @param alpha TBD
# @param tension TBD
# @param amount TBD
# @param origin TBD
#
# @return matrix
# export
get_spline <- function( xs, ys, alpha = 0.5, tension = 0.0, amount = 10, origin = TRUE ) {
	ret <- matrix(nrow = 0, ncol = 5, dimnames = list(NULL, c("idx", "frame", "type", "x", "y")), byrow = TRUE);
	ret <- data.frame(idx = numeric(), frame = numeric(), type = numeric(), x = numeric(), y = numeric());

	xs_first <- 2 * xs[1] - xs[2];
  xs_last <-  2 * xs[ length(xs)] - xs[ length(xs) - 1];
  xs <- c(xs_first, xs, xs_last);

  ys_first <- 2 * ys[1] - ys[2];
  ys_last <-  2 * ys[ length(ys)] - ys[ length(ys) - 1];
  ys <- c(ys_first, ys, ys_last);

	for( i in (2:(length(xs) - 2)) ) {
		p0 <- c( xs[i - 1], ys[i - 1] );
		p1 <- c( xs[i], ys[i] );
		p2 <- c( xs[i + 1], ys[i + 1] );
		p3 <- c( xs[i + 2], ys[i + 2] );

		t0 <- 0;
		t1 <- t0 + (dist(p0, p1) ^ alpha);
		t2 <- t1 + (dist(p1, p2) ^ alpha);
		t3 <- t2 + (dist(p2, p3) ^ alpha);
		m1x <- (1 - tension) * (t2 - t1) * ((p0[1] - p1[1]) / (t0 - t1) - (p0[1] - p2[1]) / (t0 - t2) + (p1[1] - p2[1]) / (t1 - t2));
		m1y <- (1 - tension) * (t2 - t1) * ((p0[2] - p1[2]) / (t0 - t1) - (p0[2] - p2[2]) / (t0 - t2) + (p1[2] - p2[2]) / (t1 - t2));
		m2x <- (1 - tension) * (t2 - t1) * ((p1[1] - p2[1]) / (t1 - t2) - (p1[1] - p3[1]) / (t1 - t3) + (p2[1] - p3[1]) / (t2 - t3));
		m2y <- (1 - tension) * (t2 - t1) * ((p1[2] - p2[2]) / (t1 - t2) - (p1[2] - p3[2]) / (t1 - t3) + (p2[2] - p3[2]) / (t2 - t3));
		ax <- 2 *  p1[1] - 2 * p2[1] + m1x + m2x;
		ay <- 2 *  p1[2] - 2 * p2[2] + m1y + m2y;
		bx <- -3 * p1[1] + 3 * p2[1] - 2 * m1x - m2x;
		by <- -3 * p1[2] + 3 * p2[2] - 2 * m1y - m2y;
		cx <- m1x;
		cy <- m1y;
		dx <- p1[1];
		dy <- p1[2];

		if ( origin == TRUE ) {
			ret <- rbind(ret, c(i - 1, i - 1, 1, p1) );
		}
		for ( j in (1:amount) ) {
      t <- j / amount;
      px <- ax * t * t * t + bx * t * t + cx * t + dx;
      py <- ay * t * t * t + by * t * t + cy * t + dy;

      # ret <- rbind(ret, c(i - 1, (i - 1) + (j / amount) - 0.001, 2, px, py));
      ret <- rbind(ret, c(i - 1, (i - 1) + (j / amount), ifelse(j==amount, 1, 2), px, py));
		}
	}
	if ( origin == TRUE ) {
		ret <- rbind(ret, c(i, i, 1, p2) );
	}

  colnames(ret) <-  c("idx", "frame", "type", "x", "y");
	return(ret);
}

# Title
#
# @param set TBD
# @param dims columns to mean, default: colnames(set$points)[find_dimension_cols(set$points)]
# @param group_col TBD
# @param time_col TBD
#
# @return TBD
# export
spline_points <- function(
	set,
	dims = NULL,
	group_col = "Group",
	time_col = "Time"
) {
	if(inherits(x = set, what = "ena.set")) {
		pts <- set$points;
	}
	else {
		pts <- set;
	}
	if(is.null(dims)) {
		dims <- colnames(pts)[rENA::find_dimension_cols(pts)]
	}
	if(!data.table::is.data.table(pts)) {
		pts <- data.table::as.data.table(pts);
	}
	gm <- pts[,
		lapply(.SD, mean),
		by = c(group_col, time_col),
		.SDcols = c(dims)
	][, unique(c(group_col, time_col, dims)), with = FALSE]

	class(gm) <- c("spline_points", class(gm));

	for(i in group_col) {
		class(gm[[i]]) <- c("spline_group", class( gm[[i]] ) )
		data.table::set( gm, j = i, value = gm[[i]] )
	}
	for(i in time_col) {
		class(gm[[i]]) <- c("spline_time", class( gm[[i]] ) )
		data.table::set( gm, j = i, value = gm[[i]] )
	}
	for(i in dims) {
		class(gm[[i]]) <- c("spline_dim", class( gm[[i]] ) )
		data.table::set( gm, j = i, value = gm[[i]] )
	}

	split(gm, by = group_col)
}

# Title
#
# @param sp_dat TBD
# @param x_ TBD
# @param y_ TBD
# @param opacity TBD
# @param group_col TBD
# @param smoothness TBD
# @param colors TBD
# @param all_sequences TBD
#
# @return TBD
# export
prep_splines <- function(
	sp_dat,
	x_ = "SVD1", y_ = "SVD2",
	opacity = 1,
	group_col = "Group",
	colors = RColorBrewer::brewer.pal(5, "Set2"),
	all_sequences = FALSE,
	smoothness = 10
) {
	spl_all <- lapply(sp_dat, function(xx) {
		get_spline(xx[[x_]], xx[[y_]], amount = smoothness)
	});
	spl_all_dt <- data.table::rbindlist(spl_all, idcol = "group");
	spl_all_dt$size <- (spl_all_dt$type == 1) * 10;
	spl_all_dt$opacity <- opacity;
	spl_all_dt[, c("color") := colors[as.numeric(.SD[["group"]])], .SDcols = "group"]
	spl_all_dt <- spl_all_dt[, {
    SD_one <- .SD;
    SD_one[, SD_one[seq(.GRP)], by = seq_len(nrow(.SD))]
	}, by = group]

	if(all_sequences == TRUE) {
		spl_all_dt
	}
	else {
		spl_all_dt[, max_seq := max(seq_len), by = group][ max_seq == seq_len ]
	}
}
