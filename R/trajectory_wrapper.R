#' Title
#'
#' @param dat TBD
#' @param dims TBD
#' @param mean_shape TBD
#' @param with_ci TBD
#' @param group_by TBD
#' @param split_group_on TBD
#'
#' @return plot
#' @export
trajectory_wrapper <- function(
	dat,
	dims = c(1,2),
	group_by = NULL,
	split_group_on = NULL,
	mean_shape = 15,
	with_ci = FALSE
) {
	dim_names <- colnames(as.matrix(dat))[dims];

	grp_means <- dat[, lapply(.SD, mean), by = c(group_by, split_group_on), .SDcols = c(dim_names)];
	data.table::setorderv(grp_means, c(group_by, split_group_on))
	grp_means$step <- data.table::rowidv(grp_means, cols = group_by)

	grp_mrg <- grp_means
	if( with_ci == TRUE ) {
		grp_cis <- lapply(split(dat, by = group_by, drop = TRUE), function(x) {
		  r <- split(x, by = split_group_on);
		  r2 <- lapply(r, function(s) {
		    t.test(s[, c(dim_names), with = FALSE])$conf.int
		  })
		  r2[order(as.numeric(names(r)))]
		});

		grp_cis_m <- data.table::rbindlist(lapply(grp_cis, function(x) data.frame(t(as.matrix(data.frame(x))))), idcol = TRUE);
		colnames(grp_cis_m) <- c(".id", "CI1", "CI2")
		grp_cis_m$step <- data.table::rowidv(grp_cis_m, cols = ".id")
		grp_mrg <- data.table::merge.data.table(grp_means, grp_cis_m, by.x = c(group_by, "step"), by.y = c(".id", "step"));
		grp_mrg$alpha <- scales::rescale(grp_mrg$step, to = c(0.25, 1))
	}

	g <- ggplot2::ggplot(grp_mrg, ggplot2::aes_string("step", dim_names[2], group = group_by, frame = "step", color = group_by)) +
				traj_means(mapping = ggplot2::aes(size = step, shape = mean_shape)) + traj_paths()
	;
	if ( with_ci == TRUE ) {
		g <- g + ggplot2::geom_errorbar(ggplot2::aes_string(ymin = paste0(dim_names[2], " - CI2"), ymax = paste0(dim_names[2], " + CI2"), alpha = "alpha"), data = grp_mrg, width = 0.1);
	}
	g <- g + theme_trajectories() + ggplot2::scale_shape_identity();

	g <- g + ggplot2::ggplot(grp_mrg, ggplot2::aes_string(dim_names[1], dim_names[2], group = group_by, frame = "step", color = group_by)) +
	  traj_means(ggplot2::aes(size = step, shape = mean_shape)) +  traj_paths() +
		theme_trajectories() +
  	ggplot2::scale_shape_identity()
	;

	g <- g + ggplot2::ggplot(grp_mrg, ggplot2::aes_string(dim_names[1], "step", group = group_by, frame = "step", color = group_by)) +
				traj_means(ggplot2::aes(size = step, shape = mean_shape)) +  traj_paths()
	;
	if ( with_ci == TRUE ) {
	  g <- g + ggplot2::geom_errorbar(ggplot2::aes_string(xmin = paste0(dim_names[1], " - CI1"), xmax = paste0(dim_names[1], " + CI1"), alpha = "alpha"), data = grp_mrg, width = 0.1);
	}
	g <- g + theme_trajectories() + ggplot2::scale_shape_identity();
	g <- g + trajectory_layout();

	return(g);
}
