# Trajectory ENA


## Overview

Plots epistemic networks as trajectories.

## Installation

### Install release version from CRAN

[![cran status](https://www.r-pkg.org/badges/version-ago/trajectoryENA)](https://cran.r-project.org/package=trajectoryENA) 
[![cran downloads](https://cranlogs.r-pkg.org/badges/grand-total/trajectoryENA)](https://cranlogs.r-pkg.org/badges/grand-total/trajectoryENA) 
```
Coming Soon
```

### Install development version

[![pipeline status](https://gitlab.com/epistemic-analytics/qe-packages/trajectoryENA/badges/master/pipeline.svg)](https://gitlab.com/epistemic-analytics/qe-packages/trajectoryENA/-/commits/master)
[![coverage report](https://gitlab.com/epistemic-analytics/qe-packages/trajectoryENA/badges/master/coverage.svg)](https://gitlab.com/epistemic-analytics/qe-packages/trajectoryENA/-/commits/master)
```
install.packages("trajectoryENA", repos = "https://cran.qe-libs.org")
```

## How To

### Step 1: Create an ENA Model

```
file = read.csv(system.file('extdata', 'data.csv', package = "trajectoryENA"))

set = rENA::ena(
  data = file,
  units = c("Group","Time","Speaker"),
  conversation = c("Group","Round"),
  codes = c("Disagreement","Agreement","Hypothesis","Falsifying.Evidence","Supporting.Evidence","Refutation"),
  metadata = NULL,
  model = "EndPoint",
  window.size.back = 4,
  weight.by = "binary",
  points = FALSE,
  include.plots = FALSE
);
```

### Step 2: Plot Trajectories

The `trajectory_plot` function requires a few parameters:

* The ENA model
* Column(s) representing the unique groups within the model
* Variables to use as `x` and `y` axes.  These default to SVD1 and SVD2 respectively

```
trajectory_plot(set, group_col = "Group")
```
![](man/figures/example_plot_single_1.png)

To plot the same groups over time, change either the x or y parameters. Here is SVD1 and time:

```
trajectory_plot(set, group_col = "Group", x = "SVD1", y = "Time")
```
![](man/figures/example_plot_single_2.png)

The same can be done with SVD2 and Time.  Notice, that here Time is on x to keep as SVD2 on the y-axis.

```
trajectory_plot(set, group_col = "Group", x = "Time", y = "SVD2")
```
![](man/figures/example_plot_single_3.png)

## Step 3: Putting Them Together

The real benefit of these individual plots comes when we can see them together.

Note:

_`trajectory_layout()` organizes the plots so the time plots appear in line with the SVD-only plot_
```
trajectory_plot(set, group_col = "Group", x = "Time", y = "SVD2") +
	trajectory_plot(set, group_col = "Group", x = "SVD1", y = "SVD2") +
	trajectory_plot(set, group_col = "Group", x = "SVD1", y = "Time") +
	trajectory_layout()
```
![](man/figures/example_plot_1.png)

## Step 4: Subset Plotted Groups

To look at a subset of the groups, use the `to_plot` parameter to specify which groups to include:

```
trajectory_plot(set, group_col = "Group", to_plot = c(1,2), x = "Time", y = "SVD2") +
	trajectory_plot(set, group_col = "Group", to_plot = c(1,2), x = "SVD1", y = "SVD2") +
	trajectory_plot(set, group_col = "Group", to_plot = c(1,2), x = "SVD1", y = "Time") +
	trajectory_layout()
```
![](man/figures/example_plot_2.png)

## Step 5: Increasing Point Sizes

```
trajectory_plot(set, group_col = "Group", to_plot = c(1,2), x = "Time", y = "SVD2", nodes_increasing = TRUE) +
	trajectory_plot(set, group_col = "Group", to_plot = c(1,2), x = "SVD1", y = "SVD2", nodes_increasing = TRUE) +
	trajectory_plot(set, group_col = "Group", to_plot = c(1,2), x = "SVD1", y = "Time", nodes_increasing = TRUE) +
	trajectory_layout()
```
![](man/figures/example_plot_3.png)


## Wrapper Function

```
trajectory_wrapper(set, dims = c(1,2), by = c("Group", "Time"), mean_shape = 19, with_ci = FALSE)
```
![](man/figures/example_wrapper_plot_1.png)

## Resources
To learn more about ENA, visit the [resources page](http://www.epistemicnetwork.org/resources/).

